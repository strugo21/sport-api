import dotenv from "dotenv";
import path from "path";

const NODE_ENV = process.env.NODE_ENV || 'development';

dotenv.config({
    path: path.resolve(`config/${NODE_ENV}.env`)
});

const PORT = process.env.PORT;


const DB_NAME = process.env.DB_NAME;
const DB_USER = process.env.DB_USER;
const DB_PASSWORD = process.env.DB_PASSWORD;

const JWT_KEY = process.env.JWT_KEY;

export {
    PORT,
    DB_NAME,
    DB_USER,
    DB_PASSWORD,
    JWT_KEY
};