import express, {Request, Response, NextFunction } from "express";
import { PORT} from "./config";
import router from "./api/routes";
import cors from "cors";
import handleError, { ErrorHandler } from "./api/util/handleError";


const baseUrl = '/nodejs-starter-kit';

const app = express();

/** cors options */
const corsPort = "*";
var corsOptions = {
  origin: corsPort
};

app.use(cors(corsOptions));

/** body parser */
app.use(express.urlencoded({extended: true}));
app.use(express.json());

/** public folder */
app.use(baseUrl, express.static('./public'));

/** router */
app.use(baseUrl, router);

/** not found endpoint */
app.get('*', (req: Request, res: Response) => {
    return handleError(res, new ErrorHandler(404, `${req.url} not found.`)) 
});

/** global error handling */
router.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  return handleError(res, new ErrorHandler(500, err.message, err));
});

/** server start */
app.listen(PORT, async () => {   
   console.log("Server is running")
});
