const prettier = require('prettier');
module.exports = {
    env: {
        es2020: true,
        node: true,
    },
    parser: '@typescript-eslint/parser',
    parserOptions: {
        project: 'tsconfig.json',
        ecmaVersion: 11,
        sourceType: 'module',
        ecmaFeatures: {
            modules: true,
        },
    },
    extends: [
        'airbnb-typescript/base',
        'eslint:recommended',
        'prettier',
        'plugin:@typescript-eslint/recommended',
    ],
    plugins: ['simple-import-sort', 'import', '@typescript-eslint', 'prettier'],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    overrides: [
        {
            files: ['src/**/*.ts'],
            env: {
                jest: true,
            },
        },
    ],
    ignorePatterns: ['**/dist/**/.', '**/generated/**/.', '.eslintrc.js'],
    rules: {
        'no-console': ['warn'],
        '@typescript-eslint/indent': 'off', // handled by prettier
        'no-unused-vars': 'off',
        'no-unreachable': 'error',
        'class-methods-use-this': 'off',
        'no-underscore-dangle': 'off',
        'max-classes-per-file': 'off',
        'import/order': 'off',
        'import/prefer-default-export': 'off',
        semi: 'off',
        '@typescript-eslint/sort-type-union-intersection-members': ['error'],
        '@typescript-eslint/semi': [2, 'always'],
        '@typescript-eslint/no-empty-interface': 'off',
        '@typescript-eslint/ban-ts-comment': 'off',
        '@typescript-eslint/no-unused-vars': [
            'error',
            {
                argsIgnorePattern: '^_',
            },
        ],
        'simple-import-sort/imports': [
            'error',
            {
                groups: [
                    // Side effect imports.
                    ['^\\u0000'],
                    ['^@?\\w'],
                    // Internal packages.
                    //  [^(${importPaths})(/.*|$)],
                    // Parent imports. Put .. last.
                    ['^\\.\\.(?!/?$)', '^\\.\\./?$'],
                    // Other relative imports. Put same-folder imports and . last.
                    ['^\\./(?=.*/)(?!/?$)', '^\\.(?!/?$)', '^\\./?$'],
                ],
            },
        ],
        'no-plusplus': 'off',
        'sort-imports': 'off',
        'import/first': 'warn',
        'import/newline-after-import': 'warn',
        'import/no-duplicates': 'warn',
        'no-restricted-syntax': 'off',
        '@typescript-eslint/no-non-null-assertion': 'off',
        '@typescript-eslint/explicit-module-boundary-types': 'off',
        '@typescript-eslint/no-explicit-any': 'off',
        '@typescript-eslint/naming-convention': [
            'error',
            {
                selector: 'variable',
                format: ['camelCase', 'PascalCase', 'UPPER_CASE'],
                leadingUnderscore: 'allow',
            },
            {
                selector: 'parameter',
                format: ['camelCase', 'PascalCase'],
                leadingUnderscore: 'allow',
            },
            {
                selector: 'typeLike',
                format: ['PascalCase'],
            },
            {
                selector: 'function',
                format: ['PascalCase', 'camelCase'],
            },
            {
                selector: 'classMethod',
                format: ['PascalCase', 'camelCase'],
            },
            {
                selector: 'objectLiteralProperty',
                format: ['camelCase', 'snake_case'],
            },
            {
                selector: 'objectLiteralMethod',
                format: ['camelCase'],
            },
            {
                selector: 'enumMember',
                format: ['PascalCase'],
            },
        ],
        'prettier/prettier': ['warn', { endOfLine: 'auto', semi: true }],
        'max-len': 'off',
    },
};
